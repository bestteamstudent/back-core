from collections import defaultdict

from rest_framework import serializers

from task.models import CheckList


class TaskSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    description = serializers.CharField()
    url = serializers.CharField(allow_null=True, required=False)


class CheckListSerializer(serializers.ModelSerializer):
    class Meta:
        model = CheckList
        fields = ['id', 'name', 'description', 'product_id']
