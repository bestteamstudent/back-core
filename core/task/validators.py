from rest_framework.exceptions import ValidationError


def validate_review_rating(rating: float) -> bool:
    if rating < 1 or rating > 5:
        raise ValidationError(f'Review rating < 1 or > 5')
    return True


def validate_discount(discount: int) -> bool:
    if discount < 0 or discount > 100:
        raise ValidationError(f'Discount < 0 or > 100')
    return True
