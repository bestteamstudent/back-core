from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, status
from rest_framework.renderers import JSONRenderer
from rest_framework import serializers
from rest_framework.views import APIView

from task.models import CheckList
from task.serializers import TaskSerializer, CheckListSerializer
from task.services import TasksService
from rest_framework.response import Response


# Create your views here.
class TasksView(generics.GenericAPIView):
    serializer_class = TaskSerializer
    renderer_classes = [JSONRenderer]

    class TasksRequestSerializer(serializers.Serializer):
        check_list_id = serializers.IntegerField(required=True)
        only_leaf = serializers.BooleanField(default=False)

    @swagger_auto_schema(
        query_serializer=TasksRequestSerializer(),
        responses={status.HTTP_200_OK: serializer_class()},
    )
    def get(self, request):
        serializer = self.TasksRequestSerializer(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)
        service = TasksService(**serializer.data)
        data = service.get_tasks_tree()
        return Response(data, status=200)


class CheckListView(APIView):
    class TasksRequestSerializer(serializers.Serializer):
        product_id = serializers.IntegerField(required=False)

    request_serializer = TasksRequestSerializer
    serializer_class = CheckListSerializer

    @swagger_auto_schema(query_serializer=request_serializer(), responses={200: serializer_class(many=True)})
    def get(self, request):
        check_lists = CheckList.objects.all()
        serializer = self.request_serializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        if serializer.data.get('product_id'):
            check_lists = check_lists.filter(product_id=serializer.data.get('product_id'))
        serialized_check_lists = CheckListSerializer(check_lists, many=True).data
        return Response(serialized_check_lists)
