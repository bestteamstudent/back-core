# Generated by Django 3.2.5 on 2023-11-17 13:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='checklist',
            name='version_id',
        ),
    ]
