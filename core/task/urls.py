from django.urls import path

from task import views

urlpatterns = [
    path("tasks/", views.TasksView.as_view()),
    path("checklists/", views.CheckListView.as_view())
]
