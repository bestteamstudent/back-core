from django.contrib import admin

from task.models import Task, CheckList


# Register your models here.
@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('id', "name", "description")

@admin.register(CheckList)
class CheckListAdmin(admin.ModelAdmin):
    list_display = ('id', "name", "description")