from typing import Optional

from django.db.models import QuerySet
from pydantic import BaseModel, Field

from task.models import Task


class TasksService:
    class InnerDTO(BaseModel):
        check_list_id: Optional[int] = Field(alias='check_list_id')
        only_leaf: bool

    def __init__(self, **kwargs):
        self.data = self.InnerDTO(**kwargs)

    def get_categories(self) -> QuerySet:
        categories = Task.objects.all()
        if self.data.check_list_id is not None:
            categories = Task.objects.filter(check_list_id=self.data.check_list_id)
        return categories

    def get_tasks_tree(self) -> list:
        queryset = self.get_categories()
        data = list(queryset.values("id", "name", "description", "parent_task_id", "image_url", "position"))
        for parent in data:
            parent_id = parent["id"]
            subcategories = []
            for cat in data:
                if cat["parent_task_id"] == parent_id:
                    if "subtasks" not in parent:
                        parent["subtasks"] = []
                    parent["subtasks"].append(cat)
                    subcategories.append("id")
        result = [d for d in data if d.pop("parent_task_id") is None]
        if self.data.only_leaf:
            result = [i for i in data if not i.get("subtasks")]
        return result
