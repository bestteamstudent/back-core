# Generated by Django 3.2.5 on 2023-11-17 22:57

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('versions', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='version',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
    ]
