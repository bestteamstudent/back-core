from django.contrib import admin

from versions.models import Version


# Register your models here.
@admin.register(Version)
class VersionAdmin(admin.ModelAdmin):
    list_display = ('id', "name", "description")
