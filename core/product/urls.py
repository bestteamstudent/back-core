from django.urls import path

from product import views

urlpatterns = [
    path("products/", views.ProductView.as_view()),
    path('<int:product_id>/versions/', views.VersionListAPIView.as_view(), name='product-versions'),
]
