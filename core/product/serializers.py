from collections import defaultdict

from rest_framework import serializers

from task.models import CheckList
from versions.models import Version


class ProductSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    description = serializers.CharField()
    url = serializers.CharField(allow_null=True, required=False)


class VersionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Version
        fields = '__all__'
