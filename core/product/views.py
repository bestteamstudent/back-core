from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers
from rest_framework.views import APIView

from product.models import Product
from product.serializers import ProductSerializer, VersionSerializer
from rest_framework.response import Response

from versions.models import Version


# Create your views here.
class ProductView(APIView):
    class ProductsRequestSerializer(serializers.Serializer):
        company_id = serializers.IntegerField(required=False)

    request_serializer = ProductsRequestSerializer
    serializer_class = ProductSerializer

    @swagger_auto_schema(query_serializer=request_serializer(), responses={200: serializer_class(many=True)})
    def get(self, request):
        check_lists = Product.objects.all()
        serializer = self.request_serializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        if serializer.data.get('company_id'):
            check_lists = check_lists.filter(company_id=serializer.data.get('company_id'))
        serialized_check_lists = self.serializer_class(check_lists, many=True).data
        return Response(serialized_check_lists)


class VersionListAPIView(APIView):
    def get(self, request, product_id):
        versions = Version.objects.filter(product_id=product_id).order_by('-created_at')
        serializer = VersionSerializer(versions, many=True)
        return Response(serializer.data)
