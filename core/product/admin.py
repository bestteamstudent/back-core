from django.contrib import admin

from product.models import Product


# Register your models here.
@admin.register(Product)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('id', "name", "description")