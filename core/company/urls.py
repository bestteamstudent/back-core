from django.urls import path
from .views import CompanyView

urlpatterns = [
    path('<int:company_id>/', CompanyView.as_view(), name='company-detail'),
]
