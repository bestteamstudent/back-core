from django.contrib import admin
from django.utils.safestring import mark_safe

from company.models import Company


# Register your models here.
@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('id', "picture_image", "name", "description")

    def picture_image(self, obj):
        return mark_safe(f'<img src="{obj.url or ""}" width="150" height="150" /> ')
